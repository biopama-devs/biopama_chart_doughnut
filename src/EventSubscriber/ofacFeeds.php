<?php

namespace Drupal\biopama_chart_doughnut\EventSubscriber;

use Drupal\feeds\Event\ParseEvent;
use Drupal\feeds\EventSubscriber\AfterParseBase;
use Drupal\feeds\Exception\SkipItemException;
use Drupal\feeds\Feeds\Item\ItemInterface;

/**
 * Reacts on feed imports.
 */
class ofacFeeds implements AfterParseBase {

  /**
   * {@inheritdoc}
   */
  public function applies(ParseEvent $event) {
    return $event->getFeed()->getType()->id() === 'ofac_regional_data_import';
  }

  /**
   * {@inheritdoc}
   */
  protected function alterItem(ItemInterface $item, ParseEvent $event) {
    // Conditionally skip an item.
    if (strpos($item->get('chart_type'), 'progressive_pie_chart') !== FALSE) {
      throw new SkipItemException('Do not import indicators that are not pie charts.');
    }
  }
}